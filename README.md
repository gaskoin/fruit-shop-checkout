# Fruit shop

### Things to know:
* [REST Api Description](https://bitbucket.org/gaskoin/fruit-shop-checkout/wiki/Home)
* Clients are anonymous (like in supermarket) thus purchase is not assigned to any particular client
* Every operation is performed in USD
* I've assumed the following discount logic:
   * If you buy more than ***n*** product of same type, each will be cheaper by ***$x*** 
   * If you buy products ***a*** and ***b*** toghether, each pair will be cheaper by ***$x***
* Payment as separate service, so in this component I just receive notification.
* No taxes :smile:
* You cannot weight your fruits. Each fruit is priced per item.
* Our shop has infinite warehouse. Enjoy
* It is not possible now to cancel any item from purchase.
If client have something unwanted in basket he should just say it to the person who do the scan. But if it is already scanned he need to buy it :smile:
* No product/discount management. Acceptance test handles data
* I've taken top to bottom test approach for ```PurchasingService```. 
At first point I had Integration test for it but I've found that ```CheckoutController``` tests are just a duplicate.
* Class to look at first is ```Purchase``` for domain logic, ```CheckoutController``` for REST and ```PurchasingService``` as facade between REST and domain.

### Mongo setup
> If you have ***mongo*** installed you can just run it with default configuration and skip this section

Under windows launch via ***git***/***cygwin***/***mingw*** the following script:
```
./mongo_start.sh
```
It will download and launch ***mongo*** at ```localhost:27017```

### Build

In order to run build, unit and integration tests run:
```
mvn install
```
Application is packed with spring-boot so jar is generated with dependencies.

For build and unit tests without integration test run:
```
mvn package
```

### Further Improvements:
* For more complex discount management maybe some rule engine will be more apropriate (like ***drools***)
* Pom is not big right now but in future for more verbosity maybe it can be good to move to ***Gradle***
* Mongo type converters to not write every single bit to collection (like internals of joda-money)
* Receipt class is used in domain and in REST response.
This is generally harmfull but since this class is anemic I've decided to leave this as is.
Should be refactored when complexity increase 
* ```ProductQuantityDiscount``` and ```ProductBoughtTogetherDiscount``` should be an abstraction rather than concrete classes.
These are more probable points of future extension so we can say that Open Closed principle is broken here.