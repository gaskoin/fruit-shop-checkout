package gaskoin.fruitshop.checkout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan
public class CheckoutService {
    public static void main(String[] args) {
        SpringApplication.run(CheckoutService.class);
    }
}