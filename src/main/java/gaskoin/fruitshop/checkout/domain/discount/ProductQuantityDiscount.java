package gaskoin.fruitshop.checkout.domain.discount;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import gaskoin.fruitshop.checkout.domain.purchase.PurchaseLine;
import org.joda.money.Money;

public class ProductQuantityDiscount {
    private final Product product;
    private final int minimumQuantity;
    private final Money discount;

    public ProductQuantityDiscount(Product product, int minimumQuantity, Money discount) {
        this.product = product;
        this.minimumQuantity = minimumQuantity;
        this.discount = discount;
    }

    public boolean appliesTo(PurchaseLine purchaseLine) {
        return product.equals(purchaseLine.getProduct())
                && purchaseLine.getQuantity() >= minimumQuantity;
    }

    public Money applyFor(PurchaseLine purchaseLine) {
        return discount.multipliedBy(purchaseLine.getQuantity());
    }
}
