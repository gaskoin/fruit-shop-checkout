package gaskoin.fruitshop.checkout.domain.discount;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface DiscountRepository extends MongoRepository<Discount, String> {
}