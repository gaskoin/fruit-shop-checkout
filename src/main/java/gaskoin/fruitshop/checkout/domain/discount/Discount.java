package gaskoin.fruitshop.checkout.domain.discount;

import gaskoin.fruitshop.checkout.domain.purchase.PurchaseLine;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class Discount {
    @Id
    private final String id = "discount";

    private final List<ProductQuantityDiscount> quantityDiscounts;
    private final List<ProductBoughtTogetherDiscount> boughtTogetherDiscounts;

    public Discount(List<ProductQuantityDiscount> quantityDiscounts, List<ProductBoughtTogetherDiscount> boughtTogetherDiscounts) {
        this.quantityDiscounts = quantityDiscounts;
        this.boughtTogetherDiscounts = boughtTogetherDiscounts;
    }

    public Money applyTo(List<PurchaseLine> products) {
        Money totalDiscount = Money.zero(CurrencyUnit.USD);

        for (PurchaseLine product : products) {
            Optional<ProductQuantityDiscount> discount = getFor(product);
            if (discount.isPresent()) {
                totalDiscount = totalDiscount.plus(discount.get().applyFor(product));
            }
        }

        for (ProductBoughtTogetherDiscount discount : getFor(products)) {
            totalDiscount = totalDiscount.plus(discount.applyFor(products));
        }

        return totalDiscount;
    }

    private Optional<ProductQuantityDiscount> getFor(PurchaseLine product) {
        return quantityDiscounts.stream()
                .filter(discount -> discount.appliesTo(product))
                .findFirst();
    }

    private List<ProductBoughtTogetherDiscount> getFor(List<PurchaseLine> products) {
        return boughtTogetherDiscounts.stream()
                .filter(discount -> discount.appliesTo(products))
                .collect(toList());
    }

    public static Discount noDiscount() {
        return new Discount(new ArrayList<>(), new ArrayList<>());
    }
}