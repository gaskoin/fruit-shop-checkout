package gaskoin.fruitshop.checkout.domain.discount;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import gaskoin.fruitshop.checkout.domain.purchase.PurchaseLine;
import org.joda.money.Money;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ProductBoughtTogetherDiscount {
    private final Product first;
    private final Product second;
    private final Money discount;

    public ProductBoughtTogetherDiscount(Product first, Product second, Money discount) {
        this.first = first;
        this.second = second;
        this.discount = discount;
    }

    public Money applyFor(List<PurchaseLine> purchaseLines) {
        int commonItemQuantity = Math.min(findQuantity(purchaseLines, first), findQuantity(purchaseLines, second));
        return discount.multipliedBy(commonItemQuantity);
    }

    public boolean appliesTo(List<PurchaseLine> purchaseLines) {
        List<Product> products = purchaseLines.stream()
                .map(PurchaseLine::getProduct)
                .collect(toList());

        return products.contains(first) && products.contains(second);
    }

    private int findQuantity(List<PurchaseLine> purchaseLines, Product product) {
        return purchaseLines.stream()
                .filter(purchaseLine -> purchaseLine.getProduct().equals(product))
                .map(PurchaseLine::getQuantity)
                .findFirst().get();
    }
}
