package gaskoin.fruitshop.checkout.domain.productcatalog;

import org.joda.money.Money;
import org.springframework.data.annotation.Id;

import java.util.UUID;

public class Product {
    @Id
    private final String id = UUID.randomUUID().toString();
    private final String name;
    private final Money price;

    public Product(String name, Money price) {
        this.name = name;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Money getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (!(other instanceof Product)) {
            return false;
        }

        Product product = (Product) other;
        return id.equals(product.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
