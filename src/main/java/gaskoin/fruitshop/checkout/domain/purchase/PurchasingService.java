package gaskoin.fruitshop.checkout.domain.purchase;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import gaskoin.fruitshop.checkout.domain.productcatalog.ProductRepository;
import gaskoin.fruitshop.checkout.domain.receipt.Receipt;
import gaskoin.fruitshop.checkout.domain.receipt.ReceiptPrinter;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchasingService {
    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PurchaseFactory purchaseFactory;

    public String beginCheckout() {
        Purchase purchase = purchaseFactory.createPurchase();
        purchaseRepository.save(purchase);

        return purchase.getId();
    }

    public void scanProduct(String purchaseId, String productId, int quantity) {
        Purchase purchase = findPurchase(purchaseId);
        Product product = findProduct(productId);

        purchase.add(product, quantity);

        purchaseRepository.save(purchase);
    }

    public Money getTotal(String purchaseId) {
        Purchase purchase = findPurchase(purchaseId);
        return purchase.effectiveTotal();
    }

    public void payForPurchase(String purchaseId) {
        Purchase purchase = findPurchase(purchaseId);
        purchase.checkout();

        purchaseRepository.save(purchase);
    }

    public Receipt getReceipt(String purchaseId) {
        Purchase purchase = findPurchase(purchaseId);

        return new ReceiptPrinter()
                .withBoughtProducts(purchase.getProducts())
                .withDiscount(purchase.discount())
                .withTotal(purchase.effectiveTotal())
                .print();
    }

    private Product findProduct(String productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product is not registered in catalog"));
    }

    private Purchase findPurchase(String purchaseId) {
        return purchaseRepository.findById(purchaseId)
                .orElseThrow(() -> new RuntimeException("There is no such purchase"));
    }
}