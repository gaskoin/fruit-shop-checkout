package gaskoin.fruitshop.checkout.domain.purchase;

import gaskoin.fruitshop.checkout.domain.discount.Discount;
import gaskoin.fruitshop.checkout.domain.discount.DiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static gaskoin.fruitshop.checkout.domain.discount.Discount.noDiscount;

@Service
class PurchaseFactory {
    @Autowired
    private DiscountRepository discountRepository;

    public Purchase createPurchase() {
        Discount discount = discountRepository.findAll().stream()
                .findFirst()
                .orElse(noDiscount());

        return new Purchase(discount);
    }
}
