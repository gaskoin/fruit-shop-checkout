package gaskoin.fruitshop.checkout.domain.purchase;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import org.joda.money.Money;

public class PurchaseLine {
    private final Product product;
    private int quantity;

    public PurchaseLine(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    void increaseQuantity(int quantity) {
        this.quantity += quantity;
    }

    public Money getTotal() {
        return product.getPrice().multipliedBy(quantity);
    }

    public int getQuantity() {
        return quantity;
    }
}
