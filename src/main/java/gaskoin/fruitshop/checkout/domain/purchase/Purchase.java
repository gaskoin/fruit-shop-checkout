package gaskoin.fruitshop.checkout.domain.purchase;

import gaskoin.fruitshop.checkout.domain.discount.Discount;
import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import org.joda.money.Money;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Collections.unmodifiableList;
import static org.joda.money.CurrencyUnit.USD;

public class Purchase {
    @Id
    private final String id = UUID.randomUUID().toString();
    private final Discount discount;
    private final List<PurchaseLine> products = new ArrayList<>();
    private boolean isPaid;

    public Purchase(Discount discount) {
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void add(Product product, int quantity) {
        checkIfNotFinished();

        Optional<PurchaseLine> productLine = find(product);
        if (productLine.isPresent()) {
            productLine.get().increaseQuantity((quantity));
        } else {
            addNewPurchaseLine(product, quantity);
        }
    }

    private void checkIfNotFinished() {
        if (isPaid) {
            throw new RuntimeException("Cannot add new products after payment");
        }
    }

    private Optional<PurchaseLine> find(Product product) {
        return products.stream()
                .filter(purchaseLine -> purchaseLine.getProduct().equals(product))
                .findFirst();
    }

    private void addNewPurchaseLine(Product product, int quantity) {
        products.add(new PurchaseLine(product, quantity));
    }

    public Money effectiveTotal() {
        return totalBeforeDiscount().minus(discount());
    }

    private Money totalBeforeDiscount() {
        Money total = Money.zero(USD);
        for (PurchaseLine line : products) {
            total = total.plus(line.getTotal());
        }

        return total;
    }

    public Money discount() {
        return discount.applyTo(products);
    }

    public void checkout() {
        if (isPaid) {
            throw new RuntimeException("Purchase has been paid already");
        }

        isPaid = true;
    }

    public List<PurchaseLine> getProducts() {
        return unmodifiableList(products);
    }
}