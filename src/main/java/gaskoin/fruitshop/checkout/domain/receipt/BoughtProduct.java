package gaskoin.fruitshop.checkout.domain.receipt;

import java.util.Objects;

public class BoughtProduct {
    private final String name;
    private final int quantity;
    private final String price;
    private final String total;

    private BoughtProduct() {
        this.name = "";
        this.quantity = 0;
        this.price = "";
        this.total = "";
    }

    public BoughtProduct(String name, int quantity, String price, String total) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPrice() {
        return price;
    }

    public String getTotal() {
        return total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BoughtProduct)) {
            return false;
        }

        BoughtProduct otherProduct = (BoughtProduct) o;
        return quantity == otherProduct.quantity &&
                Objects.equals(name, otherProduct.name) &&
                Objects.equals(price, otherProduct.price) &&
                Objects.equals(total, otherProduct.total);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, quantity, price, total);
    }
}
