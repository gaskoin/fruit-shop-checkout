package gaskoin.fruitshop.checkout.domain.receipt;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class Receipt {
    private final String discount;
    private final String total;
    private final List<BoughtProduct> boughtProducts;

    private Receipt(){
        this.boughtProducts = new ArrayList<>();
        this.total = "";
        this.discount = "";
    }

    public Receipt(ReceiptPrinter printer) {
        this.boughtProducts = printer.getBoughtProducts();
        this.total = printer.getTotal();
        this.discount = printer.getDiscount();
    }

    public String getDiscount() {
        return discount;
    }

    public String getTotal() {
        return total;
    }

    public List<BoughtProduct> getBoughtProducts() {
        return unmodifiableList(boughtProducts);
    }
}