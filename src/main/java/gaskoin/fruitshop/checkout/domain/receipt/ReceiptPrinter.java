package gaskoin.fruitshop.checkout.domain.receipt;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import gaskoin.fruitshop.checkout.domain.purchase.PurchaseLine;
import org.joda.money.Money;

import java.util.ArrayList;
import java.util.List;

public class ReceiptPrinter {
    private Money discount;
    private Money total;
    private List<BoughtProduct> boughtProducts = new ArrayList<>();

    public ReceiptPrinter withBoughtProducts(List<PurchaseLine> lines) {
        boughtProducts.clear();

        for (PurchaseLine line : lines) {
            Product product = line.getProduct();
            BoughtProduct boughtProduct = new BoughtProduct(
                    product.getName(),
                    line.getQuantity(),
                    product.getPrice().toString(),
                    line.getTotal().toString());
            boughtProducts.add(boughtProduct);
        }

        return this;
    }

    public ReceiptPrinter withTotal(Money total) {
        this.total = total;
        return this;
    }

    public ReceiptPrinter withDiscount(Money discount) {
        this.discount = discount;
        return this;
    }

    String getDiscount() {
        return discount.toString();
    }

    String getTotal() {
        return total.toString();
    }

    List<BoughtProduct> getBoughtProducts() {
        return boughtProducts;
    }

    public Receipt print() {
        return new Receipt(this);
    }
}
