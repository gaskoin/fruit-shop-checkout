package gaskoin.fruitshop.checkout.api;

import gaskoin.fruitshop.checkout.domain.purchase.PurchasingService;
import gaskoin.fruitshop.checkout.domain.receipt.Receipt;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class CheckoutController {
    @Autowired
    private PurchasingService purchasingService;

    @PostMapping("/checkout")
    public ResponseEntity<Void> beginCheckout(UriComponentsBuilder builder) {
        String purchaseId = purchasingService.beginCheckout();

        return ResponseEntity.status(HttpStatus.CREATED)
                .location(builder.path("/checkout/{id}").buildAndExpand(purchaseId).toUri())
                .build();
    }

    @PostMapping("/checkout/{id}/product")
    @ResponseStatus(HttpStatus.OK)
    public void scanItem(@PathVariable("id") String checkoutId, @RequestBody ProductRequest product) {
        purchasingService.scanProduct(checkoutId, product.getProductId(), product.getQuantity());
    }

    @GetMapping("/checkout/{id}/total")
    @ResponseStatus(HttpStatus.OK)
    public ActualPriceResponse getTotal(@PathVariable("id") String checkoutId) {
        Money total = purchasingService.getTotal(checkoutId);
        return new ActualPriceResponse(total.toString());
    }

    @PostMapping("/checkout/{id}/payment")
    @ResponseStatus(HttpStatus.OK)
    public void registerPayment(@PathVariable("id") String id) {
        purchasingService.payForPurchase(id);
    }

    @GetMapping("/checkout/{id}/receipt")
    @ResponseStatus(HttpStatus.OK)
    public Receipt printReceipt(@PathVariable("id") String id) {
        return purchasingService.getReceipt(id);
    }
}