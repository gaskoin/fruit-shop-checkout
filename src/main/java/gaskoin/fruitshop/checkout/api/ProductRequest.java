package gaskoin.fruitshop.checkout.api;

public class ProductRequest {
    private String productId;
    private int quantity;

    public ProductRequest() {
    }

    public ProductRequest(String productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }
}
