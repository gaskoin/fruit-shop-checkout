package gaskoin.fruitshop.checkout.api;

public class ActualPriceResponse {
    private String total;

    public ActualPriceResponse() {
    }

    public ActualPriceResponse(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }
}
