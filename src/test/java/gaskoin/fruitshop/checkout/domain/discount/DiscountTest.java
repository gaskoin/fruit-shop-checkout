package gaskoin.fruitshop.checkout.domain.discount;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import gaskoin.fruitshop.checkout.domain.purchase.PurchaseLine;
import org.joda.money.Money;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class DiscountTest {
    private final Product product = new Product("Orange", Money.parse("USD 10"));
    private final Product anotherProduct = new Product("Strawberry", Money.parse("USD 20"));
    private List<PurchaseLine> products = new ArrayList<>();

    private final Discount discount = new Discount(quantityDiscounts(), boughtTogetherDiscounts());

    @Test
    public void shouldCalculateNoDiscountForNoProducts() {
        assertEquals(Money.parse("USD 0"), discount.applyTo(products));
    }

    @Test
    public void shouldCalculateNoDiscountIfNoCriteriaMet() {
        addProduct(product, 1);

        assertEquals(Money.parse("USD 0"), discount.applyTo(products));
    }

    @Test
    public void shouldCalculateDiscountForMultipleProducts() {
        addProduct(product, 10);

        assertEquals(Money.parse("USD 30"), discount.applyTo(products));
    }

    @Test
    public void shouldCalculateDiscountForCombinedProducts() {
        addProduct(product, 2);
        addProduct(anotherProduct, 3);

        assertEquals(Money.parse("USD 16"), discount.applyTo(products));
    }

    @Test
    public void shouldCalculateMixedDiscounts() {
        addProduct(product, 5);
        addProduct(anotherProduct, 3);

        assertEquals(Money.parse("USD 39"), discount.applyTo(products));
    }

    private void addProduct(Product product, int quantity) {
        products.add(new PurchaseLine(product, quantity));
    }

    private List<ProductQuantityDiscount> quantityDiscounts() {
        return singletonList(new ProductQuantityDiscount(product, 3, Money.parse("USD 3")));
    }

    private List<ProductBoughtTogetherDiscount> boughtTogetherDiscounts() {
        return singletonList(new ProductBoughtTogetherDiscount(product, anotherProduct, Money.parse("USD 8")));
    }
}