package gaskoin.fruitshop.checkout.domain.discount;

import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import org.joda.money.Money;

import java.util.List;

import static java.util.Collections.singletonList;

public class DiscountSample {
    public static Discount forProducts(Product product, Product anotherProduct) {
        return new Discount(quantityDiscountsFor(product), boughtTogetherDiscounts(product, anotherProduct));
    }

    private static List<ProductQuantityDiscount> quantityDiscountsFor(Product product) {
        return singletonList(new ProductQuantityDiscount(product, 3, Money.parse("USD 3")));
    }

    private static List<ProductBoughtTogetherDiscount> boughtTogetherDiscounts(Product product, Product anotherProduct) {
        return singletonList(new ProductBoughtTogetherDiscount(product, anotherProduct, Money.parse("USD 8")));
    }
}
