package gaskoin.fruitshop.checkout.domain.purchase;

import gaskoin.fruitshop.checkout.domain.discount.Discount;
import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PurchaseTest {
    private final Product product = new Product("Orange", Money.parse("USD 10"));
    private final Discount discount = mock(Discount.class);
    private final Purchase purchase = new Purchase(discount);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void beforeEachTest() {
        when(discount.applyTo(anyList())).thenReturn(Money.zero(CurrencyUnit.USD));
    }

    @Test
    public void shouldReturnZeroPriceWhenNoProductsAdded() {
        assertEquals(Money.zero(CurrencyUnit.USD), purchase.effectiveTotal());
    }

    @Test
    public void shouldCalculateTotalPriceForProduct() {
        purchase.add(product, 2);

        assertEquals(Money.parse("USD 20"), purchase.effectiveTotal());
    }

    @Test
    public void shouldIncreaseQuantityOfTheSameProduct() {
        purchase.add(product, 1);
        purchase.add(product, 2);

        assertEquals(1, purchase.getProducts().size());
        assertEquals(3, purchase.getProducts().get(0).getQuantity());
    }

    @Test
    public void shouldTakeDiscountIntoAccountWhenCalculatingTotalPrice() {
        when(discount.applyTo(anyList())).thenReturn(Money.parse("USD 20"));

        purchase.add(product, 6);

        assertEquals(Money.parse("USD 40"), purchase.effectiveTotal());
    }

    @Test
    public void shouldFailToAddProductAfterCheckout() {
        purchase.checkout();

        exception.expectMessage("Cannot add new products after payment");
        purchase.add(product, 1);
    }

    @Test
    public void shouldFailToPayTwice() {
        purchase.add(product, 1);
        purchase.checkout();

        exception.expectMessage("Purchase has been paid already");
        purchase.checkout();
    }
}