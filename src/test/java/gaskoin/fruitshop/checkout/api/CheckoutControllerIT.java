package gaskoin.fruitshop.checkout.api;

import gaskoin.fruitshop.checkout.CheckoutService;
import gaskoin.fruitshop.checkout.domain.discount.DiscountRepository;
import gaskoin.fruitshop.checkout.domain.discount.DiscountSample;
import gaskoin.fruitshop.checkout.domain.productcatalog.Product;
import gaskoin.fruitshop.checkout.domain.productcatalog.ProductRepository;
import gaskoin.fruitshop.checkout.domain.receipt.BoughtProduct;
import gaskoin.fruitshop.checkout.domain.receipt.Receipt;
import org.joda.money.Money;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = WebEnvironment.RANDOM_PORT,
        classes = CheckoutService.class)
public class CheckoutControllerIT {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DiscountRepository discountRepository;

    private Product orange;
    private Product banana;
    private String checkout;

    @Before
    public void beforeEachTest() {
        orange = productRepository.save(new Product("Orange", Money.parse("USD 10")));
        banana = productRepository.save(new Product("Banana", Money.parse("USD 20")));
        discountRepository.save(DiscountSample.forProducts(orange, banana));

        checkout = restTemplate.postForLocation("/checkout", null).toString();
    }

    @Test
    public void shouldScanItemAndReturnActualPrice() {
        scan(orange, 2);

        assertThat(getActualPrice().getTotal(), equalTo("USD 20.00"));
    }

    @Test
    public void shouldDiscountMultipleProducts() {
        scan(orange, 73);

        assertThat(getActualPrice().getTotal(), equalTo("USD 511.00"));
    }

    @Test
    public void shouldDiscountCombinedProducts() {
        scan(orange, 10);
        scan(banana, 15);

        assertThat(getActualPrice().getTotal(), equalTo("USD 290.00"));
    }

    @Test
    public void shouldReceiveReceiptAfterPayment() {
        scan(orange, 12);
        scan(banana, 30);
        pay();

        Receipt receipt = restTemplate.getForObject(checkout + "/receipt", Receipt.class);

        List<BoughtProduct> expectedBoughtProducts = Arrays.asList(
                new BoughtProduct("Orange", 12, "USD 10.00", "USD 120.00"),
                new BoughtProduct("Banana", 30, "USD 20.00", "USD 600.00")
        );
        assertThat(receipt.getBoughtProducts(), equalTo(expectedBoughtProducts));
        assertThat(receipt.getTotal(), equalTo("USD 588.00"));
        assertThat(receipt.getDiscount(), equalTo("USD 132.00"));
    }

    private void scan(Product product, int quantity) {
        restTemplate.postForObject(checkout + "/product", new ProductRequest(product.getId(), quantity), Void.class);
    }

    private ActualPriceResponse getActualPrice() {
        return restTemplate.getForObject(checkout + "/total", ActualPriceResponse.class);
    }

    private void pay() {
        restTemplate.postForObject(checkout + "/payment", null, Void.class);
    }
}