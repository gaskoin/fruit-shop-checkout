#!/bin/bash

set -e

if [ ! -d mongodb-3.6.3 ]; then
  curl 'http://downloads.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-3.6.3.zip' -o mongo.zip
  unzip mongo.zip -d .
  mv mongodb-win32-x86_64-2008plus-ssl-3.6.3 mongodb-3.6.3
  rm -rf mongo.zip
fi

mkdir -p target/data/db
mongodb-3.6.3/bin/mongod --dbpath target/data/db